//
//  UICollectionView+.swift
//  Weather
//
//  Created by Rose Maina on 07/08/2023.
//

import UIKit

class ContentHeightCollection: UICollectionView {

    override var contentSize: CGSize {
        didSet {
            self.invalidateIntrinsicContentSize()
        }
    }

    override var intrinsicContentSize: CGSize {
        return CGSize(width: contentSize.width, height: contentSize.height)
    }
}

/// Set the cell's idenfier to the file name
extension UICollectionViewCell {
    static var identifier: String {
        return String(describing: self)
    }
}
