//
//  UIStackView+.swift
//  Weather
//
//  Created by Rose Maina on 08/08/2023.
//

import UIKit

extension UIStackView {
    static func createStackView(alignment: UIStackView.Alignment,
                                distribution: UIStackView.Distribution,
                                spacing: CGFloat,
                                axis: NSLayoutConstraint.Axis) -> UIStackView {
        
          let stackview = UIStackView()
          stackview.axis = axis
          stackview.distribution = distribution
          stackview.alignment = alignment
          stackview.spacing = spacing
          stackview.translatesAutoresizingMaskIntoConstraints = false

          return stackview
      }
}
