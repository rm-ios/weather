//
//  String+.swift
//  Weather
//
//  Created by Rose Maina on 08/08/2023.
//

import Foundation

extension String {
    func localized(bundle: Bundle = Bundle.main) -> String {
        return NSLocalizedString(self, bundle: bundle, comment: "\(self)_comment")
    }
}
