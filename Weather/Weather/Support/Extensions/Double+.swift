//
//  Double+.swift
//  Weather
//
//  Created by Rose Maina on 06/08/2023.
//

import Foundation

extension Double {
    func roundDouble() -> String {
        return String(format: "%.0f", self)
    }
}
