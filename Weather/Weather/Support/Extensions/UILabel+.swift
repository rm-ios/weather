//
//  UILabel+.swift
//  Weather
//
//  Created by Rose Maina on 08/08/2023.
//

import UIKit

extension UILabel {
    static func createLabel(title: String = "",
                            textColor: UIColor = .black,
                            font: UIFont = UIFont.systemFont(ofSize: 13,
                                                             weight: .regular),
                            alignment: NSTextAlignment = .left) -> UILabel {
       
        let label = UILabel()
        label.text = title
        label.font = font
        label.textColor = textColor
        label.textAlignment = alignment
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }
}
