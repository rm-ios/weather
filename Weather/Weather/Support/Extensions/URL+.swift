//
//  URL+.swift
//  Weather
//
//  Created by Rose Maina on 07/08/2023.
//

import Foundation

extension URL {
    func appending(_ queryItems: [URLQueryItem]) -> URL? {
        guard
            var urlComponents = URLComponents(url: self, resolvingAgainstBaseURL: true)
        else { return nil }
        
        urlComponents.queryItems = (urlComponents.queryItems ?? []) + queryItems
        return urlComponents.url
    }
}
