//
//  NetworkManager.swift
//  Weather
//
//  Created by Rose Maina on 06/08/2023.
//

import Foundation

class NetworkManager {
    
    static let shared = NetworkManager()
    
    private init() {}
    
    func request<T: Codable>(from url: String) async throws -> T {
        guard let url = URL(string: url) else {
            throw WeatherError.invalidURL
        }
        
        let (data, response) = try await URLSession.shared.data(from: url)
        
        guard let response = response as? HTTPURLResponse, response.statusCode == 200 else {
            throw WeatherError.invalidResponse
        }
        
        let JSONString = String(data: data, encoding: String.Encoding.utf8)!
        print(JSONString)
        
        do {
            return try JSONDecoder().decode(T.self, from: data)
        } catch {
            throw WeatherError.invalidData
        }
    }
    
    func handleErrors(error: Error) -> String {
        switch error {
        case WeatherError.invalidData:
            return "Invalid data received from the server"
        case WeatherError.invalidResponse:
            return "Invalid Response"
        case WeatherError.invalidURL:
            return "Invalid URL"
        default:
            return "An unknnown error occurred"
        }
    }
}

public enum WeatherError: Error {
    case invalidData
    case invalidResponse
    case invalidURL
}
