//
//  NetworkManagerMock.swift
//  Weather
//
//  Created by Rose Maina on 07/08/2023.
//

import Foundation

class NetworkManagerMock: NetworkManagerProtocol {
    
    var mockWeather: WeatherModel?
    var mockForecast: ForecastModel?
    var city: ForecastModel?
    
    func request<T: Codable>(from url: URL?) async throws -> T {
        if T.self == WeatherModel.self, let weather = mockWeather as? T {
            return weather
        } else if T.self == ForecastModel.self, let forecast = mockForecast as? T {
            return forecast
        } else if T.self == CitySearchModel.self, let city = city as? T {
            return city
        } else {
            throw WeatherError.invalidData
        }
    }
    
    func handleErrors(error: Error) -> String {
        switch error {
        case WeatherError.invalidData:
            return "Invalid data received from the server"
        case WeatherError.invalidResponse:
            return "Invalid Response"
        case WeatherError.invalidURL:
            return "Invalid URL"
        default:
            return "An unknnown error occurred"
        }
    }
}
