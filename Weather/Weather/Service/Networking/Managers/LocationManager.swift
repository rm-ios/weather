//
//  LocationManager.swift
//  Weather
//
//  Created by Rose Maina on 06/08/2023.
//

import CoreLocation

class LocationManager: NSObject, ObservableObject {
    
    let manager = CLLocationManager()
    
    @Published var location: CLLocationCoordinate2D?
    @Published var isLoading = false
    @Published var showErrorAlert = false
    
    override init() {
        super.init()
        setupLocationManager()
    }
    
    func setupLocationManager() {
        manager.delegate = self
        manager.desiredAccuracy = kCLLocationAccuracyBest
        manager.allowsBackgroundLocationUpdates = false
    }
    
    /// Check if User has enabled Locations in "Settings App"
    func checkLocationServices() {
        DispatchQueue.global().async { [weak self] in
            guard let self = self else { return }
            
            if CLLocationManager.locationServicesEnabled() {
                self.checkLocationAuthorization()
            } else {
                self.showErrorAlert = true
            }
        }
    }
    
    /// Check if User has enabled any authorization of Locations
    func checkLocationAuthorization() {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            
            switch self.manager.authorizationStatus {
            case .authorizedWhenInUse, .authorizedAlways:
                self.isLoading = true
                self.manager.requestLocation()
            default:
                self.manager.requestWhenInUseAuthorization()
            }
        }
    }
}

// MARK: - Location Manager Delegate Methods
extension LocationManager: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        location = locations.first?.coordinate
        isLoading = false
        manager.stopUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        isLoading = false
        print("Error getting location", error)
    }
}
