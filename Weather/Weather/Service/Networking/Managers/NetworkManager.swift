//
//  NetworkManager.swift
//  Weather
//
//  Created by Rose Maina on 06/08/2023.
//

import Foundation

protocol NetworkManagerProtocol {
    func request<T: Codable>(from url: URL?) async throws -> T
    func handleErrors(error: Error) -> String
}

class NetworkManager: NetworkManagerProtocol {
    
    /// Make Instances confirm protocol instead of using Class
    static let shared: NetworkManagerProtocol = NetworkManager()
    
    /// Create session that can be create during  testing
    let session: URLSession
    
    init(session: URLSession = .shared) {
        self.session = session
    }
       
    /// Run Generic Async/ Await Call for network APIs
    func request<T: Codable>(from url: URL?) async throws -> T {
        guard let fullURL = url else { throw WeatherError.invalidURL }
        
        let (data, response) = try await session.data(from: fullURL)
        
        guard let response = response as? HTTPURLResponse, response.statusCode == 200 else {
            throw WeatherError.invalidResponse
        }
        
        do {
            return try JSONDecoder().decode(T.self, from: data)
        } catch {
            throw WeatherError.invalidData
        }
    }
    
    /// Handle URL Request Possible Errors
    func handleErrors(error: Error) -> String {
        switch error {
        case WeatherError.invalidData:
            return "Invalid data received from the server"
        case WeatherError.invalidResponse:
            return "Invalid Response"
        case WeatherError.invalidURL:
            return "Invalid URL"
        default:
            return "An unknnown error occurred"
        }
    }
}
