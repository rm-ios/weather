//
//  WeatherModel.swift
//  Weather
//
//  Created by Rose Maina on 06/08/2023.
//

import Foundation

// MARK: - Current Weather Details Model
struct WeatherModel: Codable, Equatable {
    let coordinates: Coordinates
    let weather: [Weather]
    let main: WeatherDetails
    let wind: WindModel
    let system: SystemModel?
    let timezone: Int
    let id: Int
    let name: String
    let code: Int
    
    enum CodingKeys: String, CodingKey {
        case coordinates = "coord"
        case weather = "weather"
        case main = "main"
        case wind = "wind"
        case system = "sys"
        case timezone = "timezone"
        case id = "id"
        case name = "name"
        case code = "cod"
    }
}

// MARK: - Coordinates Model
struct Coordinates: Codable, Equatable {
    let latitude, longitude: Double
    
    enum CodingKeys: String, CodingKey {
        case latitude = "lat"
        case longitude = "lon"
    }
}

// MARK: - Weather Model
struct Weather: Codable, Equatable {
    let id: Int
    let description, icon, main: String
}

// MARK: - Weather Details Model
struct WeatherDetails: Codable, Equatable {

    let temperature, feelsLike, minTemp, maxTemp: Double
    let pressure, humidity, seaLevel, groundLevel: Int
    
    enum CodingKeys: String, CodingKey {
        case feelsLike = "feels_like"
        case groundLevel = "grnd_level"
        case humidity = "humidity"
        case maxTemp = "temp_max"
        case minTemp = "temp_min"
        case pressure = "pressure"
        case seaLevel = "sea_level"
        case temperature = "temp"
    }
}

// MARK: -  Wind Model
struct WindModel: Codable, Equatable {
    let degrees: Int
    let gust, speed: Double
    
    enum CodingKeys: String, CodingKey {
        case degrees = "deg"
        case gust = "gust"
        case speed = "speed"
    }
}

// MARK: - System Model
struct SystemModel: Codable, Equatable {
    let country: String
}
