//
//  CitySearchModel.swift
//  Weather
//
//  Created by Rose Maina on 08/08/2023.
//

import Foundation

struct CitySearchModel: Codable {
   
    let name: String
    let latitude, longitude: Double
    
    enum CodingKeys: String, CodingKey {
        case name = "name"
        case latitude = "lat"
        case longitude = "lon"
    }
}
