//
//  ForecastModel.swift
//  Weather
//
//  Created by Rose Maina on 07/08/2023.
//

import Foundation

// MARK: - Weather Forecast Model
struct ForecastModel: Codable, Equatable {
    
    let code: String
    let message, timestampCount: Int
    let forecastList: [ForecastList]
    let city: City
    
    enum CodingKeys: String, CodingKey {
        case code = "cod"
        case message = "message"
        case timestampCount = "cnt"
        case forecastList = "list"
        case city = "city"
    }
}

// MARK: - City Details
struct City: Codable, Equatable {
    
    let id: Int
    let name, country: String
    let coordinates: Coordinates
    let population, timezone: Int
    let sunrise, sunset: Int
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case name = "name"
        case coordinates = "coord"
        case country = "country"
        case population = "population"
        case timezone = "timezone"
        case sunrise = "sunrise"
        case sunset = "sunset"
    }
}

// MARK: -  Weather Forecast List Details
struct ForecastList: Codable, Identifiable, Equatable {
    
    var id = UUID()
    let forecastedTime: Int
    let main: ForecastDetails
    let weather: [Weather]
    let wind: WindModel
    let visibility: Int
    let precpProbalility: Double
    let rain: ForecastRain?
    let sys: PartOfDay
    let dateTime: String
    
    enum CodingKeys: String, CodingKey {
        case forecastedTime = "dt"
        case main = "main"
        case weather = "weather"
        case wind = "wind"
        case visibility = "visibility"
        case precpProbalility = "pop"
        case rain = "rain"
        case sys = "sys"
        case dateTime = "dt_txt"
    }
}

// MARK: - Weather Forecast Details
struct ForecastDetails: Codable, Equatable {
   
    let temperature, feelsLike, minTemp, maxTemp: Double
    let pressure, humidity, seaLevel, groundLevel: Int
    let internalTemp: Double
    
    enum CodingKeys: String, CodingKey {
        case feelsLike = "feels_like"
        case groundLevel = "grnd_level"
        case humidity = "humidity"
        case maxTemp = "temp_max"
        case minTemp = "temp_min"
        case pressure = "pressure"
        case seaLevel = "sea_level"
        case temperature = "temp"
        case internalTemp = "temp_kf"
    }
}

// MARK: - Rain Weather Forecast Details
struct ForecastRain: Codable, Equatable {
    let rainValue: Double
    
    enum CodingKeys: String, CodingKey {
        case rainValue = "3h"
    }
}

// MARK: - Part Of Day Forecast Details
struct PartOfDay: Codable, Equatable {
    let partOfDay: String
    
    enum CodingKeys: String, CodingKey {
        case partOfDay = "pod"
    }
}
