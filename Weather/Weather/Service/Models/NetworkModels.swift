//
//  NetworkModels.swift
//  Weather
//
//  Created by Rose Maina on 08/08/2023.
//

import Foundation

// MARK: - API Response Errors
public enum WeatherError: Error {
    case invalidData
    case invalidResponse
    case invalidURL
}

// MARK: - API URLs
struct Endpoints {
    static let forecastAPI = "https://api.openweathermap.org/data/2.5/forecast"
    static let weatherAPI = "https://api.openweathermap.org/data/2.5/weather"
    static let geoCodingApi = "http://api.openweathermap.org/geo/1.0/direct?"
    static let appID = "cf0e0f438b9d3aef3362de9691d6066a"
}
