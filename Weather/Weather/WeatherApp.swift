//
//  WeatherApp.swift
//  Weather
//
//  Created by Rose Maina on 06/08/2023.
//

import SwiftUI

@main
struct WeatherApp: App {
    var body: some Scene {
        WindowGroup {
            RootScreen()
        }
    }
}
