//
//  BasicButton.swift
//  Weather
//
//  Created by Rose Maina on 07/08/2023.
//

import SwiftUI

/// Classic Button with a filled color and text
struct BasicButton: View {
    
    var backgroundColor: Color
    var textColor: Color
    var title: String
    
    var body: some View {
        Text(title)
            .frame(width: 280, height: 40)
            .background(backgroundColor)
            .foregroundColor(textColor)
            .font(.system(size: 17,
                          weight: .bold,
                          design: .default))
            .cornerRadius(20)
            .opacity(0.8)
    }
}

struct BasicButton_Previews: PreviewProvider {
    static var previews: some View {
        BasicButton(backgroundColor: .green,
                    textColor: .white,
                    title: "Button Title")
    }
}
