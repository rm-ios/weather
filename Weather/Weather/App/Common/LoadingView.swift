//
//  LoadingView.swift
//  Weather
//
//  Created by Rose Maina on 06/08/2023.
//

import SwiftUI

/// Progress bar for Requests Loading
struct LoadingView: View {
    
    var body: some View {
        ProgressView()
            .progressViewStyle(CircularProgressViewStyle(tint: .white))
            .frame(maxWidth: .infinity, maxHeight: .infinity)
    }
}

struct LoadingView_Previews: PreviewProvider {
    static var previews: some View {
        LoadingView()
    }
}
