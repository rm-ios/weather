//
//  ErrorAlertSheet.swift
//  Weather
//
//  Created by Rose Maina on 08/08/2023.
//

import SwiftUI

/// Basic Custom Alert Sheet with a title and CTA
struct ErrorAlertSheet: View {
    
    var btnTitle: String
    var callAction: () -> Void
    var errorMessage: String
    
    var body: some View {
        VStack {
            Text(errorMessage)
                .font(.body)
                .foregroundColor(.secondary)
                .multilineTextAlignment(.center)
            
            Button(action: callAction ) {
                BasicButton(backgroundColor: .red,
                            textColor: .white,
                            title: btnTitle)
            }
        }
    }
}

struct ErrorAlertSheet_Previews: PreviewProvider {
    static var previews: some View {
        ErrorAlertSheet(btnTitle: "Done",
                        callAction: {},
                        errorMessage: "Something went wrong. Try again later.")
    }
}
