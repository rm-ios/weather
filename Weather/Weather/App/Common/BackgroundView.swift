//
//  SwiftUIView.swift
//  Weather
//
//  Created by Rose Maina on 06/08/2023.
//

import SwiftUI

/// Gives a uniform look for the app's main background
struct BackgroundView: View {
    var body: some View {
        LinearGradient(gradient: Gradient(colors: [.blue, Color("sky")]),
                       startPoint: .topLeading,
                       endPoint: .bottomTrailing)
        .edgesIgnoringSafeArea(.all)
    }
}

struct SwiftUIView_Previews: PreviewProvider {
    static var previews: some View {
        BackgroundView()
    }
}
