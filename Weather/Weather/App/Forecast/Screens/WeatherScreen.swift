//
//  WeatherScreen.swift
//  Weather
//
//  Created by Rose Maina on 06/08/2023.
//

import SwiftUI

enum WeatherType: String {
    case rainy = "Rain"
    case cloudy = "Cloudy"
    case sunny = "Sunny"
}

struct WeatherScreen: View {
    
    var forecast: ForecastModel
    var weather: WeatherModel
    
    @State private var showWeatherDetailsScreen = false
    
    var body: some View {
        NavigationView {
            VStack {
                Image(returnBackgroundImage(type: weather.weather.first?.main))
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .frame(maxWidth: .infinity, maxHeight: 400)
                    .overlay {
                        VStack {
                            Text(weather.main.temperature.roundDouble() + "°")
                                .font(.system(size: 50, weight: .bold))
                                .foregroundColor(.white)
                            
                            Text(weather.weather.first?.main ?? "")
                                .font(.system(size: 30, weight: .semibold))
                                .foregroundColor(.white)
                                .textCase(.uppercase)
                        }
                    }
                    .edgesIgnoringSafeArea(.all)
                
                TempStatusView(backgroundColor: returnBackgroundColor(type: weather.weather.first?.main),
                               currentTemp:"\(weather.main.temperature.roundDouble())",
                               minTemp: "\(weather.main.minTemp.roundDouble())",
                               maxTemp: "\(weather.main.maxTemp.roundDouble())")
                
                Divider()
                
                List(forecast.forecastList) { forecast in
                    WeatherDayView(backgroundColor: returnBackgroundColor(type: weather.weather.first?.main),
                                   dayOfWeek: returnDayOfWeek(date: forecast.dateTime),
                                   imageName: returnForecastIcon(type: forecast.weather.first?.main),
                                   temperature: "\(forecast.main.temperature.roundDouble())")
                    .listRowSeparator(.hidden)
                    .listRowBackground(Color(returnBackgroundColor(type: weather.weather.first?.main)))
                }
                .frame(maxWidth: .infinity)
                .edgesIgnoringSafeArea([.leading,.trailing])
                .listStyle(.plain)
                
                Button {
                    showWeatherDetailsScreen.toggle()
                    
                } label: {
                    BasicButton(backgroundColor: .white,
                                textColor: Color(returnBackgroundColor(type: weather.weather.first?.main)),
                                title: "browse_title".localized())
                }
                
                Spacer()
            }
            .background(Color(returnBackgroundColor(type: weather.weather.first?.main)))
            .background(
                NavigationLink(destination: HomeSearchScreen(),
                               isActive: $showWeatherDetailsScreen) { EmptyView() }
            )
        }
        .navigationBarBackButtonHidden(true)
    }
    
    /// Given more time, I can make these reusable functions
    func returnForecastIcon(type: String?) -> String {
        var imageName = ""
        
        if let type = type {
            switch type {
            case "Clouds":
                imageName = "cloudy_icon"
            case "Rain":
                imageName = "rain_icon"
            case "Clear":
                imageName = "sunny_icon"
            default: break
            }
        }
        
        return imageName
    }
    
    func returnBackgroundColor(type: String?) -> String {
        var color = ""
        
        if let type = type {
            switch type {
            case "Clouds":
                color = "cloudy"
            case "Rain":
                color = "rainy"
            case "Clear":
                color = "sunny"
            default: break
            }
        }
        
        return color
    }
    
    func returnBackgroundImage(type: String?) -> String {
        var imageName = ""
        
        if let type = type {
            switch type {
            case "Clouds":
                imageName = "forest_cloudy"
            case "Rain":
                imageName = "forest_rainy"
            case "Clear":
                imageName = "forest_sunny"
            default: break
            }
        }
        
        return imageName
    }
    
    func returnDayOfWeek(date: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE"
        
        let pickedDate = dateFormatter.date(from: date)
        return dateFormatter.string(from: pickedDate ?? Date())
    }
}

struct WeatherScreen_Previews: PreviewProvider {
    static var previews: some View {
        WeatherScreen(
            forecast: ForecastModel(code: "200",
                                    message: 0,
                                    timestampCount: 40,
                                    forecastList: [ForecastList(forecastedTime: 1661871600,
                                                                main: ForecastDetails(temperature: 296.76,
                                                                                      feelsLike: 296.98,
                                                                                      minTemp: 296.76,
                                                                                      maxTemp: 297.87,
                                                                                      pressure: 1015,
                                                                                      humidity: 69,
                                                                                      seaLevel: 1015,
                                                                                      groundLevel: 933,
                                                                                      internalTemp: -1.11),
                                                                weather: [Weather(id: 500,
                                                                                  description: "light rain",
                                                                                  icon: "10d",
                                                                                  main: "Rain")],
                                                                wind: WindModel(degrees: 349,
                                                                                gust: 1.18,
                                                                                speed: 0.62),
                                                                visibility: 10000,
                                                                precpProbalility: 0.32,
                                                                rain: nil,
                                                                sys: PartOfDay(partOfDay: "d"),
                                                                dateTime: "2022-08-30 15:00:00"),
                                                   
                                                   ForecastList(forecastedTime: 1661871600,
                                                                main: ForecastDetails(temperature: 296.76,
                                                                                      feelsLike: 296.98,
                                                                                      minTemp: 296.76,
                                                                                      maxTemp: 297.87,
                                                                                      pressure: 1015,
                                                                                      humidity: 69,
                                                                                      seaLevel: 1015,
                                                                                      groundLevel: 933,
                                                                                      internalTemp: -1.11),
                                                                weather: [Weather(id: 500,
                                                                                  description: "light rain",
                                                                                  icon: "10d",
                                                                                  main: "Rain")],
                                                                wind: WindModel(degrees: 349,
                                                                                gust: 1.18,
                                                                                speed: 0.62),
                                                                visibility: 10000,
                                                                precpProbalility: 0.32,
                                                                rain: nil,
                                                                sys: PartOfDay(partOfDay: "d"),
                                                                dateTime: "2022-08-30 15:00:00"),
                                                   
                                                   ForecastList(forecastedTime: 1661871600,
                                                                main: ForecastDetails(temperature: 296.76,
                                                                                      feelsLike: 296.98,
                                                                                      minTemp: 296.76,
                                                                                      maxTemp: 297.87,
                                                                                      pressure: 1015,
                                                                                      humidity: 69,
                                                                                      seaLevel: 1015,
                                                                                      groundLevel: 933,
                                                                                      internalTemp: -1.11),
                                                                weather: [Weather(id: 500,
                                                                                  description: "light rain",
                                                                                  icon: "10d",
                                                                                  main: "Rain")],
                                                                wind: WindModel(degrees: 349,
                                                                                gust: 1.18,
                                                                                speed: 0.62),
                                                                visibility: 10000,
                                                                precpProbalility: 0.32,
                                                                rain: nil,
                                                                sys: PartOfDay(partOfDay: "d"),
                                                                dateTime: "2022-08-30 15:00:00")
                                    ],
                                    city: City(id: 3163858,
                                               name: "Zocca",
                                               country: "IT",
                                               coordinates: Coordinates(latitude: 44.34,
                                                                        longitude: 10.99),
                                               population: 4593,
                                               timezone: 7200,
                                               sunrise: 1661834187,
                                               sunset: 1661882248)),
            
            weather: WeatherModel(coordinates: Coordinates(latitude: 44.43,
                                                           longitude: 10.99),
                                  weather: [Weather(id: 800,
                                                    description: "clear sky",
                                                    icon: "01n",
                                                    main: "Clear")],
                                  main: WeatherDetails(temperature: 291.68,
                                                       feelsLike: 291.47,
                                                       minTemp: 289.74,
                                                       maxTemp: 292.54,
                                                       pressure: 1008,
                                                       humidity: 72,
                                                       seaLevel: 1008,
                                                       groundLevel: 958),
                                  wind: WindModel(degrees: 201,
                                                  gust: 4.7,
                                                  speed: 3.66),
                                  system: SystemModel(country: "IT"),
                                  timezone: 7200,
                                  id: 3175676,
                                  name: "Guiglia",
                                  code: 200))
    }
}
