//
//  WeatherDetailsScreen.swift
//  Weather
//
//  Created by Rose Maina on 07/08/2023.
//

import SwiftUI

struct WeatherDetailsScreen: View {
    var weather: WeatherModel
    
    var body: some View {
        ZStack(alignment: .leading) {
            VStack {
                VStack(alignment: .leading, spacing: 5) {
                    Text(weather.name)
                        .bold().font(.title)
                    
                    Text("Today \(Date().formatted(.dateTime.month().day().hour().minute()))")
                        .fontWeight(.light)
                }
                .frame(maxWidth: .infinity, alignment: .leading)
                
                Spacer()
                
                VStack {
                    HStack {
                        
                        VStack(spacing: 20) {
                            Image(systemName: "cloud.rain.fill")
                                .font(.system(size: 40))
                            
                            Text(weather.weather[0].main)
                        }
                        .frame(width: 150, alignment: .leading)
                        
                        Spacer()
                        
                        Text(weather.main.feelsLike.roundDouble() + "°")
                            .font(.system(size: 100))
                            .fontWeight(.bold)
                            .padding()
                    }
                    
                    Spacer()
                        .frame(height: 250)
                    
                }
                .frame(maxWidth: .infinity)
            }
            .padding()
            .frame(width: .infinity, alignment: .leading)
            
            
            
            VStack {
                Spacer()
                
                VStack(alignment: .leading, spacing: 20) {
                    Text("Weather Now")
                        .bold().padding(.bottom)
                    
                    HStack{
                        WeatherRow(logo: "thermometer", name: "Min temp", value: (weather.main.minTemp.roundDouble() + "°"))
                        Spacer()
                        WeatherRow(logo: "thermometer", name: "Max temp", value: (weather.main.maxTemp.roundDouble() + "°"))
                    }
                    HStack{
                        WeatherRow(logo: "wind", name: "Wind Speed", value: (weather.wind.speed.roundDouble() + "m/s"))
                        Spacer()
                        WeatherRow(logo: "humidity", name: "Humidity", value: (String(weather.main.humidity) + "%"))
                    }
                }
                .frame(maxWidth: .infinity, alignment: .leading)
                .padding()
                .padding(.bottom, 20)
                .foregroundColor(Color(hue: 0.656, saturation: 0.787, brightness: 0.354))
                .background(.white)
                .cornerRadius(20, corners: [.topLeft, .topRight])
            }
            
            
        }
        .edgesIgnoringSafeArea(.bottom)
        .background(Color(hue: 0.656, saturation: 0.787, brightness: 0.354))
        .preferredColorScheme(.dark)
    }
}

struct WeatherDetailsScreen_Previews: PreviewProvider {
    static var previews: some View {
        WeatherDetailsScreen(weather: WeatherModel(coordinates: Coordinates(latitude: 44.43,
                                                                            longitude: 10.99),
                                                   weather: [Weather(id: 800,
                                                                     description: "clear sky",
                                                                     icon: "01n",
                                                                     main: "Clear")],
                                                   main: WeatherDetails(temperature: 291.68,
                                                                        feelsLike: 291.47,
                                                                        minTemp: 289.74,
                                                                        maxTemp: 292.54,
                                                                        pressure: 1008,
                                                                        humidity: 72,
                                                                        seaLevel: 1008,
                                                                        groundLevel: 958),
                                                   wind: WindModel(degrees: 201,
                                                                   gust: 4.7,
                                                                   speed: 3.66),
                                                   system: SystemModel(country: "IT"),
                                                   timezone: 7200,
                                                   id: 3175676,
                                                   name: "Guiglia",
                                                   code: 200))
    }
}
