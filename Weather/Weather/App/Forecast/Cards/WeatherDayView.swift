//
//  WeatherDayView.swift
//  Weather
//
//  Created by Rose Maina on 07/08/2023.
//

import SwiftUI

/// Returns a Forecast View with the Day, Weather Image and Temperature
struct WeatherDayView: View {
    
    var backgroundColor: String
    var dayOfWeek: String
    var imageName: String
    var temperature: String
    
    var body: some View {
        HStack {
            Text(dayOfWeek)
                .font(.system(size: 16, weight: .medium))
                .foregroundColor(.white)
                .padding(.leading, 20)
            
            Spacer()
            
            Image(imageName)
                .symbolRenderingMode(.multicolor)
                .resizable()
                .aspectRatio(contentMode: .fit)
                .frame(width: 40, height: 40)
            
            Spacer()
            
            Text("\(temperature)°")
                .font(.system(size: 16, weight: .medium))
                .foregroundColor(.white)
                .padding(.trailing, 20)
        }
        .frame(maxWidth: .infinity)
        .background(Color(backgroundColor))
    }
}

struct WeatherDayView_Previews: PreviewProvider {
    static var previews: some View {
        WeatherDayView(backgroundColor: "sky",
                       dayOfWeek: "Wednesday",
                       imageName: "sunny_icon",
                       temperature: "270")
    }
}
