//
//  TempStatusView.swift
//  Weather
//
//  Created by Rose Maina on 07/08/2023.
//

import SwiftUI

/// Horizontal Temperature View
struct TempStatusView: View {
    
    var backgroundColor: String
    var currentTemp: String
    var minTemp: String
    var maxTemp: String
    
    var body: some View {
        HStack {
            TemperatureView(tempTitle: "min_text".localized(),
                            tempValue: minTemp)
            .padding(.leading, 20)
            
            Spacer()
            
            TemperatureView(tempTitle: "current_text".localized(),
                            tempValue: currentTemp)
            
            Spacer()
            
            TemperatureView(tempTitle: "max_text".localized(),
                            tempValue: maxTemp)
            .padding(.trailing, 20)
        }
        .frame(maxWidth: .infinity, maxHeight: 80)
        .background(Color(backgroundColor))
    }
}

struct TempStatusView_Previews: PreviewProvider {
    static var previews: some View {
        TempStatusView(backgroundColor: "sky",
                       currentTemp: "279",
                       minTemp: "271",
                       maxTemp: "290")
    }
}
