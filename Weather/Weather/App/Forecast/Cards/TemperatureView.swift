//
//  TemperatureView.swift
//  Weather
//
//  Created by Rose Maina on 07/08/2023.
//

import SwiftUI

/// Vertical Reusable View with title and description
struct TemperatureView: View {
    
    var tempTitle: String
    var tempValue: String
    
    var body: some View {
        VStack {
            Text("\(tempValue)º")
                .font(.system(size: 16, weight: .regular))
                .foregroundColor(.white)
            
            Text(tempTitle)
                .font(.system(size: 15, weight: .regular))
                .foregroundColor(.white)
        }
    }
}

struct TemperatureView_Previews: PreviewProvider {
    static var previews: some View {
        TemperatureView(tempTitle: "current", tempValue: "21")
    }
}
