//
//  WeatherViewModel.swift
//  Weather
//
//  Created by Rose Maina on 07/08/2023.
//

import Foundation

@MainActor final class WeatherViewModel: ObservableObject {
    
    @Published private(set) var errorMessage: String?
    @Published private(set) var forecast: ForecastModel?
    @Published private(set) var weather: WeatherModel?
        
    // MARK: - Get Current Weather Details at a specified Location
    func getCurrentWeather(lat: String, lon: String) async throws {
        let url = URL(string: Endpoints.weatherAPI)
        
        let fullURL = url?.appending([URLQueryItem(name: "lat", value: lat),
                                     URLQueryItem(name: "lon", value: lon),
                                     URLQueryItem(name: "appid", value: Endpoints.appID)])
        
        guard let validURL = fullURL else { throw WeatherError.invalidURL }
        
        do {
            weather = try await NetworkManager.shared.request(from: validURL)
        } catch {
            errorMessage = NetworkManager.shared.handleErrors(error: error)
        }
    }
    
    // MARK: - Get 5 Day Weather Forecast at a specified Location
    func getForecastWeather(lat: String, lon: String) async throws {
        let url = URL(string: Endpoints.forecastAPI)
        
        let fullURL = url?.appending([URLQueryItem(name: "lat", value: lat),
                                     URLQueryItem(name: "lon", value: lon),
                                     URLQueryItem(name: "appid", value: Endpoints.appID)])
        
        guard let validURL = fullURL else { throw WeatherError.invalidURL }
        
        do {
            forecast = try await NetworkManager.shared.request(from: validURL)
        } catch {
            errorMessage = NetworkManager.shared.handleErrors(error: error)
        }
    }
}
