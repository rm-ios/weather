//
//  RootScreen.swift
//  Weather
//
//  Created by Rose Maina on 06/08/2023.
//

import SwiftUI
import CoreLocation

struct RootScreen: View {
    
    @State var errorMessage: String?
    @State var showErrorAlert = false
    
    @State var forecast: ForecastModel?
    @State var weather: WeatherModel?
    
    @StateObject private var locationManager = LocationManager()
    @StateObject private var viewModel = WeatherViewModel()
    
    var body: some View {
        ZStack {
            BackgroundView()
            
            VStack {
                if let location = locationManager.location {
                    if let weather = viewModel.weather, let forecast = viewModel.forecast {
                        WeatherScreen(forecast: forecast,
                                      weather: weather)
                    } else {
                        LoadingView()
                            .task {
                                do {
                                    let latitude = String(location.latitude.rounded()).replacingOccurrences(of: "-", with: "")
                                    let longitude = String(location.longitude.rounded()).replacingOccurrences(of: "-", with: "")
                                   
                                   try await viewModel.getCurrentWeather(lat: latitude,
                                                                          lon: longitude)
                                    
                                    try await viewModel.getForecastWeather(lat: latitude,
                                                                           lon: longitude)
                                } catch {
                                    errorMessage = NetworkManager.shared.handleErrors(error: error)
                                    showErrorAlert.toggle()
                                }
                            }
                    }
                } else {
                    if locationManager.isLoading {
                        LoadingView()
                    } else {
                        WelcomeView()
                            .environmentObject(locationManager)
                    }
                }
            }
            .sheet(isPresented: $showErrorAlert, content: {
                ErrorAlertSheet(btnTitle: "okay".localized(),
                                callAction: { showErrorAlert.toggle() },
                                errorMessage: errorMessage ?? "basic_error".localized()
                )
                .presentationDetents([.fraction(0.2)])
                .presentationDragIndicator(.visible)
            })
        }
    }
}

struct RootScreen_Previews: PreviewProvider {
    static var previews: some View {
        RootScreen()
    }
}
