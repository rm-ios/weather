//
//  WelcomeView.swift
//  Weather
//
//  Created by Rose Maina on 06/08/2023.
//

import SwiftUI
import CoreLocationUI

struct WelcomeView: View {
    
    @EnvironmentObject var locationManager: LocationManager
    
    var body: some View {
        VStack(spacing: 20) {
            Text("Weather")
                .bold()
                .font(.title)
                .foregroundColor(.white)
            
            Text("share_location".localized())
                .multilineTextAlignment(.center)
                .foregroundColor(.white)
                .padding([.leading, .trailing], 40)
            
            LocationButton(.shareCurrentLocation) {
                locationManager.checkLocationServices()
            }
            .cornerRadius(30)
            .symbolVariant(.fill)
            .foregroundColor(.white)
        }
        .sheet(isPresented: $locationManager.showErrorAlert, content: {
            ErrorAlertSheet(btnTitle: "okay".localized(),
                            callAction: { locationManager.showErrorAlert.toggle() },
                            errorMessage: "disabled_location_error".localized()
            )
            .presentationDetents([.fraction(0.2)])
            .presentationDragIndicator(.visible)
        })
    }
}

struct WelcomeView_Previews: PreviewProvider {
    static var previews: some View {
        WelcomeView()
    }
}
