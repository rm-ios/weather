//
//  HomeSearchScreen.swift
//  Weather
//
//  Created by Rose Maina on 07/08/2023.
//

import SwiftUI

struct HomeSearchScreen: View {
    
    var body: some View {
        VStack {
            NavigationView {
                HomeSearchRepresentable()
            }
            .navigationTitle("city_weather".localized())
            .navigationBarTitleDisplayMode(.automatic)
        }
        .ignoresSafeArea(.all)
    }
}

struct HomeSearchScreen_Previews: PreviewProvider {
    static var previews: some View {
        HomeSearchScreen()
    }
}
