//
//  WeatherDetailsScene.swift
//  Weather
//
//  Created by Rose Maina on 07/08/2023.
//

import UIKit

class WeatherDetailsScene: UIViewController {
    
    // MARK: - Instance Properties
    /// Given more time, I can extect the properties into a view model
    var errorMessage: String?
    var weather: WeatherModel?
    
    var cityList = [CitySearchModel]()
    var isSearch : Bool = false
    
    // MARK: - Computed Prperties
    lazy var searchBar: UISearchBar = {
        let searchBar = UISearchBar()
        searchBar.searchBarStyle = .default
        searchBar.isTranslucent = false
        searchBar.delegate = self
        searchBar.translatesAutoresizingMaskIntoConstraints = false
        navigationItem.titleView = searchBar
        return searchBar
    }()
    
    lazy var resultsCollection: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        
        let collectionView = ContentHeightCollection(frame: view.frame,
                                                     collectionViewLayout: layout)
        collectionView.backgroundColor = .clear
        collectionView.showsVerticalScrollIndicator = false
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        return collectionView
    }()
    
    // MARK: - Override Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        configureViews()
    }
    
    private func setupCollectionView() {
        resultsCollection.delegate = self
        resultsCollection.dataSource = self
        resultsCollection.register(CityWeatherCollectionCell.self,
                                   forCellWithReuseIdentifier: CityWeatherCollectionCell.identifier)
    }
    
    private func resignSearchBar() {
        searchBar.resignFirstResponder()
        searchBar.showsCancelButton = false
        searchBar.placeholder = "search_city".localized()
        isSearch = false
    }
    
    private func getCityName(name: String) {
        Task {
            do {
                try await getCoordinatesFromCity(cityName: name)
            } catch {
                showErrorAlert()
            }
        }
        
        resignSearchBar()
    }
    
    private func getCurrentWeatherByCity() async {
        if let city = cityList.first {
            do {
                try await getCurrentWeather(lat: city.latitude.roundDouble(),
                                            lon: city.latitude.roundDouble())
            } catch {
                print("Not getting any results")
            }
        }
    }
}

// MARK: - Configure Views
extension WeatherDetailsScene {
    private func configureViews() {
        configureSubviews()
        configureLayout()
    }
    
    private func configureSubviews() {
        [searchBar, resultsCollection].forEach { view.addSubview($0) }
        
        setupCollectionView()
    }
    
    private func configureLayout() {
        NSLayoutConstraint.activate([
            searchBar.topAnchor.constraint(equalTo: view.topAnchor, constant: 10),
            searchBar.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 10),
            searchBar.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -10),
            
            resultsCollection.topAnchor.constraint(equalTo: searchBar.bottomAnchor, constant: 15),
            resultsCollection.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 15),
            resultsCollection.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -15),
            resultsCollection.heightAnchor.constraint(equalToConstant: 550),
        ])
    }
}

// MARK: - Collection View Delegation and Setup
extension WeatherDetailsScene: UICollectionViewDataSource, UICollectionViewDelegate {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CityWeatherCollectionCell.identifier,
                                                            for: indexPath) as? CityWeatherCollectionCell
        else { return UICollectionViewCell() }
        
        if let weather = weather {
            cell.configureCell(with: weather)
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("getting selected")
    }
}

// MARK: - Setup Collection's items in the grid
extension WeatherDetailsScene: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout
                        collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let cellWidth = (collectionView.bounds.width) - 20
        return CGSize(width: cellWidth, height: 100)
    }
}

// MARK: - Search Bar Delegates
extension WeatherDetailsScene: UISearchBarDelegate {
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        isSearch = true
        searchBar.showsCancelButton = true
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        isSearch = (searchText.count == 0) ? false : true
        self.resultsCollection.reloadData()
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        resignSearchBar()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard let city = searchBar.text, !city.isEmpty else {
            searchBar.showsCancelButton = true
            return
        }
        
        self.getCityName(name: city)
    }
}

// MARK: - Alerts
extension WeatherDetailsScene {
    func showErrorAlert() {
        let alert = basicAlert(title: "Error",
                               message: errorMessage ?? "Something went wrong try again later")
        alert.addAction(UIAlertAction(title: "Cancel", style: .destructive))
        self.present(alert, animated: true, completion: nil)
    }
    
    /// Given more time, I can extract this into a reusable customisable view
    func basicAlert(title: String, message: String) ->  UIAlertController {
        let alert = UIAlertController(title: title,
                                      message: message,
                                      preferredStyle: .alert)
        return alert
    }
}

// MARK: - APIs
/// Given more time, I can extect this into its own view model
extension WeatherDetailsScene{
    // MARK: - Get Current Weather Details at a specified Location
    func getCurrentWeather(lat: String, lon: String) async throws {
        let url = URL(string: Endpoints.weatherAPI)
        
        let fullURL = url?.appending([URLQueryItem(name: "lat", value: lat),
                                      URLQueryItem(name: "lon", value: lon),
                                      URLQueryItem(name: "appid", value: Endpoints.appID)])
        
        do {
            weather = try await NetworkManager.shared.request(from: fullURL)
        } catch {
            errorMessage = NetworkManager.shared.handleErrors(error: error)
        }
    }
    
    // MARK: - Get Location Coordinates using Cty Names
    /// Search Morroco, London
    func getCoordinatesFromCity(cityName: String) async throws {
        let url = URL(string: Endpoints.geoCodingApi)
        
        let fullURL = url?.appending([URLQueryItem(name: "q", value: cityName),
                                      URLQueryItem(name: "limit", value: "2"),
                                      URLQueryItem(name: "appid", value: Endpoints.appID)])
        
        do {
            cityList = try await NetworkManager.shared.request(from: fullURL)
            
            if let city = cityList.first {
                do {
                    try await getCurrentWeather(lat: city.latitude.roundDouble(),
                                                lon: city.latitude.roundDouble())
                    resultsCollection.reloadData()
                    
                } catch {
                    print("Not getting any results")
                }
            }
            
        } catch {
            errorMessage = NetworkManager.shared.handleErrors(error: error)
        }
    }
}
