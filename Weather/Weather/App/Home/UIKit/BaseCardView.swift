//
//  BaseViewController.swift
//  Weather
//
//  Created by Rose Maina on 08/08/2023.
//

import UIKit
import SwiftUI

class BaseCardView: UIView {
   
    // MARK: - Override Methods
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadWithBackground()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func loadWithBackground() {
        let hostScreen = UIHostingController(rootView: BackgroundView())
        
        addSubview(hostScreen.view)

        hostScreen.view.translatesAutoresizingMaskIntoConstraints = false

        layer.cornerRadius = 10
        
        NSLayoutConstraint.activate([
            hostScreen.view.topAnchor.constraint(equalTo: topAnchor),
            hostScreen.view.leadingAnchor.constraint(equalTo: leadingAnchor),
            hostScreen.view.trailingAnchor.constraint(equalTo: trailingAnchor),
            hostScreen.view.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
    }
}
