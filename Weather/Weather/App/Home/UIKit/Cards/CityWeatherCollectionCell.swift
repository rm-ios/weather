//
//  CityWeatherCollectionCell.swift
//  Weather
//
//  Created by Rose Maina on 08/08/2023.
//

import UIKit

class CityWeatherCollectionCell: UICollectionViewCell {
    
    // MARK: - Instance Properties
    let contentHolder: UIView = {
        let view = BaseCardView()
        
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.cornerRadius = 10
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOffset = CGSize(width: 0, height: 1)
        view.layer.shadowOpacity = 0.16
        view.backgroundColor = .lightText
    
        return view
    }()
    
    // Left Views
    let leftStack = UIStackView.createStackView(alignment: .leading,
                                                distribution: .fillProportionally,
                                                spacing: 5,
                                                axis: .vertical)
    
    var cityLabel = UILabel.createLabel(title: "Miami",
                                        font: UIFont.systemFont(ofSize: 30,
                                                                weight: .bold))
    
    var weatherDescLabel = UILabel.createLabel(title: "moderaten rain",
                                               font: UIFont.systemFont(ofSize: 15,
                                                                       weight: .medium))
    
    var weatherIcon: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "sunny_icon")
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    // Right Views
    let rightStack = UIStackView.createStackView(alignment: .trailing,
                                                 distribution: .fillProportionally,
                                                 spacing: 5,
                                                 axis: .vertical)
    
    var tempLabel = UILabel.createLabel(title: "79º",
                                        font: UIFont.systemFont(ofSize: 30,
                                                                weight: .semibold))
    
    var pressureLabel = UILabel.createLabel(title: "Pressure: 1014",
                                            font: UIFont.systemFont(ofSize: 13,
                                                                    weight: .medium))
    
    var windLabel = UILabel.createLabel(title: "Wind: 11.99",
                                        font: UIFont.systemFont(ofSize: 13,
                                                                weight: .medium))
    
    // MARK: - Initializers
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    // MARK: - Setup Cell Properties
    private func setup() {
        addSubview(contentHolder)
        
        [leftStack, weatherIcon, rightStack].forEach { contentHolder.addSubview($0) }
        [cityLabel, weatherDescLabel].forEach { leftStack.addArrangedSubview($0) }
        [tempLabel, pressureLabel, windLabel].forEach { rightStack.addArrangedSubview($0) }
        
        NSLayoutConstraint.activate([
            contentHolder.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 5),
            contentHolder.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -5),
            contentHolder.topAnchor.constraint(equalTo: topAnchor, constant: 5),
            contentHolder.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -5),
            
            leftStack.topAnchor.constraint(equalTo: contentHolder.topAnchor, constant: 10),
            leftStack.leadingAnchor.constraint(equalTo: contentHolder.leadingAnchor, constant: 15),
            leftStack.widthAnchor.constraint(equalTo: contentHolder.widthAnchor, multiplier: 0.4),
            leftStack.bottomAnchor.constraint(equalTo: contentHolder.bottomAnchor, constant: -10),
            
            weatherIcon.centerYAnchor.constraint(equalTo: contentHolder.centerYAnchor),
            weatherIcon.centerXAnchor.constraint(equalTo: contentHolder.centerXAnchor),
            weatherIcon.widthAnchor.constraint(equalToConstant: 50),
            weatherIcon.heightAnchor.constraint(equalToConstant: 50),
            
            rightStack.topAnchor.constraint(equalTo: contentHolder.topAnchor, constant: 10),
            rightStack.trailingAnchor.constraint(equalTo: contentHolder.trailingAnchor, constant: -15),
            rightStack.widthAnchor.constraint(equalTo: contentHolder.widthAnchor, multiplier: 0.4),
            rightStack.bottomAnchor.constraint(equalTo: contentHolder.bottomAnchor, constant: -10),
        ])
    }
    
    func configureCell(with model: WeatherModel) {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.cityLabel.text = model.name
            self.weatherDescLabel.text = model.weather.first?.description
            self.tempLabel.text = "\(model.main.temperature.roundDouble())º"
            self.pressureLabel.text = "Pressure: \(model.main.pressure)"
            self.windLabel.text =  "Wind: \(model.wind.speed.roundDouble())"
            self.weatherIcon.image = UIImage(named: self.returnWeatherIcon(type: model.weather.first?.main))
        }
    }
    
    func returnWeatherIcon(type: String?) -> String {
        var imageName = ""
        
        if let type = type {
            switch type {
            case "Clouds":
                imageName = "cloudy_icon"
            case "Rain":
                imageName = "rain_icon"
            case "Clear":
                imageName = "sunny_icon"
            default: break
            }
        }
        
        return imageName
    }
}
