//
//  HomeSearchRepresentable.swift
//  Weather
//
//  Created by Rose Maina on 07/08/2023.
//

import SwiftUI

/// Manage UIViewController Scene in Swift UI Interface
struct HomeSearchRepresentable: UIViewControllerRepresentable {
    func makeUIViewController(context: Context) -> WeatherDetailsScene {
        return WeatherDetailsScene()
    }

    func updateUIViewController(
        _ uiViewController: WeatherDetailsScene, context: Context
    ) {}
}
