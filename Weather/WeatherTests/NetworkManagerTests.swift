//
//  NetworkManagerTests.swift
//  WeatherTests
//
//  Created by Rose Maina on 07/08/2023.
//

import XCTest
@testable import Weather

@MainActor final class NetworkManagerTests: XCTestCase {
    
    /// Test the Current Weather in user's location
    func testGetCurrentWeather() async {
        let viewModel = WeatherViewModel()
        let mockManager = NetworkManagerMock()
        
        mockManager.mockWeather = MockData.weather
        
        do {
            try await viewModel.getCurrentWeather(lat: "44.43", lon: "10.99")
            XCTAssertEqual(viewModel.weather?.coordinates, MockData.weather.coordinates)
        } catch {
            XCTFail("Expected failure with \(error)")
        }
    }
    
    /// Test Weather Forecast for a user's location for the Next 5 Days
    func testGetForecastWeather() async {
        let viewModel = WeatherViewModel()
        let mockManager = NetworkManagerMock()
        
        mockManager.mockForecast = MockData.forecast
        
        do {
            try await viewModel.getForecastWeather(lat: "44.43", lon: "10.99")
            XCTAssertEqual(viewModel.forecast?.city.name, MockData.forecast.city.name)
        } catch {
            XCTFail("Expected failure with \(error)")
        }
    }
}
