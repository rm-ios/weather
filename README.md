![Language](https://img.shields.io/badge/language-Swift-orange.svg)

# Swift UI Weather App

This is a weather app built using Swift UI. The application gets a user's current location coordinates and uses them to view for current weather and the weather forecast of the next several days based off that location.

## Features
- Display current weather based on user's location
- Show weather forecase fot the next 5 days.
- Search for a city
- Display current weather based on a city.

## Get Started
To clone and run this application, you'll need [Xcode14+](https://apps.apple.com/ke/app/xcode/id497799835?mt=12) installed on your computer. From your command line:

```bash
1. Clone this repository.
$ https://gitlab.com/rm-ios/weather.git
```
```
2. Open the project in Xcode.
$ cd weather
```

```
3. Before you run the project, change your bundle identifier and team name to match your own.
     Check Link - https://drive.google.com/file/d/1PXeuw-WeBVlR6lOaTa-QwrtO3qqNJ9E3/view?usp=sharing
```

```swift
4. Replace `YOURAPIKEY` with your valid Open Weather Map API key in struct `Endpoints`

struct Endpoints {
    static let appid = "YOURAPIKEY"
}
```

```
5. Build and run the app on a simulator or device.
```

## Dependencies
1. [SwiftUI](https://developer.apple.com/documentation/swiftui/): Offered a modern and declarative approach to building user interface.

2. [UIKit](https://developer.apple.com/documentation/uikit/): With its mature ecosystem, it offered the flexibility and familiarity to create polished user interface.

3. [CoreLocation](https://developer.apple.com/documentation/corelocation/): Accessed the user's current geographical location in order to get weather status and forecasts based on that location.

4. [CoreLocationUI](https://developer.apple.com/documentation/corelocationui): Contains a standardized UI that interacts securely with Core Location to request authorization to access location data. It provided `LocationButton` to grant one-time authorization for the app to fetch the user's location.

5. [XCTest](https://developer.apple.com/documentation/xctest/): Helped create and run unit tests for the weather app.

## Technologies
> Open Weather Map APIs
1. [Current Weather](https://openweathermap.org/current#geo) - Access current weather data for any location.
2. [Weather Forecast ](https://openweathermap.org/forecast5#geo5) - A 5 day forecast is available at any location.

## Considerations
#### Coding Style
- Avoid unnecessary code: Extract repeating code or functionalty be resusable.
- Naming constants and variables: Use camel case and name alphabetically with group separations or easier reading.
- Spacing: Place modifiers and extra long lines of code into new lines for easy reading.
- Closure syntax: Use trailing closure syntax wherever possible, and prefer to use the closure parameters descriptive names.
- Generics: Use upper camel case names. When a type name doesn't have a meaningful relationship or role, use a traditional single uppercase letter. ie T.
- Code Organization: Prefer extensions to organize code into logical blocks of functionality such delegate methods or configuring views and using `//MARK:-` comment to define what the block is.
- Memory Management: Use `weak` or `unowned` to prevent strong cycles.

#### Architecture
The application is built using the MVVM (Model-View-ViewModel) architecture, which ensures separation of concerns and promotes testability.
- Model: Represents the data.
- View: Renders the user interface based on ViewModel's data.
- ViewModel: Handles data presentation and interaction.

#### Swift Guidelines
Prioritized clean, concise, and expressive Swift code. Considered some of below best practices:
- Write self-documenting code with descriptive naming and clear comments.
- Keep functions and classes focused on a single responsibility.
- Avoid force unwrapping and use optional binding and chaining.

#### General
- Async/Await: Used to perform asynchronous tasks efficiently and avoid blocking the main thread.
- Protocols and Dependency Injection: Used to define interfaces and enable dependency injection to enhance testability, and modularity in the application.
- Delegation: Used to communicate between different components, facilitating loose coupling and better separation of responsibilities.
- Singleton: Used sparingly to manage global state, ensuring a single instance across the application.
- Abstraction and Subclassing: Abstraction guided the design of classes and their relationships, and subclassing promoted code reuse and specialization.
- Interoperability with UIKit: Where required, the project leveraged UIKit alongside SwiftUI using the UIViewRepresentable and UIViewControllerRepresentable protocols.

#### Comments
- Testing: Could use `URLProtocol` to improve code testability.

## Media
1. [Screenshots](https://docs.google.com/document/d/1iXM9C2PRiKGcduH2DP1VZGhYnqcP2UhmzCpHwcqy_lI/edit?usp=sharing)
2. [Demo](https://drive.google.com/drive/folders/1nzBHAS5PNBR5OLIB_iE90Ll8mzBrm6Fz?usp=sharing)

